#!/bin/bash

set -x

SYSCTL=/usr/sbin/sysctl
IP=/usr/sbin/ip
TC=/usr/sbin/tc
IPTABLES=/usr/sbin/iptables
BUSYBOX=/usr/bin/busybox
IPERF=/usr/bin/iperf3

# Common Part
${SYSCTL} -w net.ipv4.ip_forward=1

# Set up NAT routing
${IPTABLES} -t nat -A POSTROUTING -o eno8 -j MASQUERADE

# Set up Local Internet!
${IP} link add mynet type dummy
${IP} addr add 2.2.2.2/32 dev mynet
${IP} link set mynet up

# Start IPERF Server
${IPERF3} -s -B 2.2.2.2 -D

for N in 1 2 3 4 5 6 
do
	if [ "$#" -eq "0" ]
	then
		${IP} netns add S${N}
		${IP} netns exec S${N} ${IP} addr add 127.0.0.1/8 dev lo
		${IP} netns exec S${N} ${IP} link set lo up
		${IP} netns exec S${N} ${IP} link add name T${N} type bridge
		# ${IP} netns exec S${N} ${SYSCTL} -w net.ipv4.conf.T${N}.arp_ignore=8
		${IP} netns exec S${N} ${IP} link set T${N} up

		${IP} link add W${N} type veth peer name U${N}
		${IP} link set W${N} up

		${IP} addr add 172.20.${N}.1/24 dev W${N}
		${BUSYBOX} udhcpd /vm/dhcp/W${N}.conf

		${IP} link set U${N} netns S${N}
		${IP} netns exec S${N} ${IP} link set U${N} up
		${IP} netns exec S${N} ${IP} link set U${N} master T${N}

		${IP} link set eno${N} netns S${N}
		${IP} netns exec S${N} ${IP} link set eno${N} up
		${IP} netns exec S${N} ${IP} link set eno${N} master T${N}
	else
		${IP} link set eno${N} up
		${IP} addr add 172.20.${N}.1/24 dev eno${N}
		${BUSYBOX} udhcpd /vm/dhcp/eno${N}.conf
	fi
done

# WAN impairments
if [ "$#" -eq "0" ]
then
	# Wired -- no impairments

	# Wi-Fi -- 1ms, 100Mb/s, 1%
	${IP} netns exec S2 ${TC} qdisc add dev eno2 root netem delay 1ms rate 100mbit loss 1%
	${IP} netns exec S2 ${TC} qdisc add dev U2   root netem delay 1ms rate 100mbit loss 1%

	# LTE -- 15ms, 50 Mb/s, 1%
	${IP} netns exec S3 ${TC} qdisc add dev eno3 root netem delay 15ms rate 50mbit loss 1%
	${IP} netns exec S3 ${TC} qdisc add dev U3   root netem delay 15ms rate 50mbit loss 1%

	# VSAT -- 250ms, 10 Mb/s, 2%
	${IP} netns exec S4 ${TC} qdisc add dev eno4 root netem delay 250ms rate 10mbit loss 2%
	${IP} netns exec S4 ${TC} qdisc add dev U4   root netem delay 250ms rate 10mbit loss 2%

	# LTE -- 15ms, 50 Mb/s, 1%
	${IP} netns exec S5 ${TC} qdisc add dev eno5 root netem delay 15ms rate 50mbit loss 1%
	${IP} netns exec S5 ${TC} qdisc add dev U5   root netem delay 15ms rate 50mbit loss 1%

	# VSAT -- 250ms, 10 Mb/s, 2%
	${IP} netns exec S6 ${TC} qdisc add dev eno6 root netem delay 250ms rate 10mbit loss 2%
	${IP} netns exec S6 ${TC} qdisc add dev U6   root netem delay 250ms rate 10mbit loss 2%
fi

# The LAN Side
${IP} addr add 10.0.254.3/24 dev eno7
${IP} link set eno7 up

for V in 4093 10 20 30
do
	${IP} link add link eno7 name V${V} type vlan id ${V}
	${IP} link add name B${V} type bridge
	${IP} link set V${V} master B${V}
	${SYSCTL} -w net.ipv4.conf.B${V}.arp_ignore=8

	${IP} link set B${V} up
	${IP} link set V${V} up
done

# The Devices
cd /vm/device

for Q in *
do
	${IP} netns add D${Q}
	${IP} link add E${Q} type veth peer name F${Q}

	if [ -f /vm/device/${Q}/vlan ]
	then
		${IP} link set E${Q} master B`cat /vm/device/${Q}/vlan`
	else
		${IP} link set E${Q} master B4093
	fi
	${IP} link set E${Q} up

	# after this lin we can't see interface F in the main namespace
	${IP} link set F${Q} netns D${Q}

	if [ -f /vm/device/${Q}/mac ]
	then
		MAC=`cat /vm/device/${Q}/mac`
	else
		MAC="02:CA:FE:00:00:${Q}"
		echo ${MAC} > /vm/device/${Q}/mac
	fi

	${IP} netns exec D${Q} ${IP} link set F${Q} addr ${MAC}
	${IP} netns exec D${Q} ${IP} link set F${Q} up

	${IP} netns exec D${Q} ${IP} addr add 127.0.0.1/8 dev lo
	${IP} netns exec D${Q} ${IP} link set lo up
done

