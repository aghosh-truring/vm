#!/bin/bash

set -x 

cd /vm/device

Q=${1:?"Needs Parameter"}

if [ ! -d /vm/device/${Q} ]
then
	echo "No such device"
	exit 2
fi

ip netns exec D${Q} dhcpcd -k F${Q}
