#!/bin/bash

set -x 

cd /vm/device

for Q in *
do
	ip netns exec D${Q} dhcpcd -k F${Q}
done
