#!/bin/sh

/vm/old-qemu-system-x86_64 -boot d  -cdrom  debian-10.1.0-amd64-netinst.iso -hda /vm/debian1.qcow2 -name VM1  --enable-kvm  -m 8192 -cpu host -smp 4 \
	-device e1000,netdev=mynet0,mac=DE:AD:BE:EF:25:10 -netdev tap,id=mynet0,ifname=tap1,script=no,downscript=no \
	-device e1000,netdev=mynet1,mac=DE:AD:CE:EF:25:11 -netdev tap,id=mynet1,ifname=tap2,script=no,downscript=no \
	-device e1000,netdev=mynet3,mac=DE:AD:CE:EF:25:13 -netdev tap,id=mynet3,ifname=tap3,script=no,downscript=no \
	-device e1000,netdev=mynet4,mac=DE:AD:CE:EF:25:14 -netdev tap,id=mynet4,ifname=tap4,script=no,downscript=no \
	-device e1000,netdev=mynet5,mac=DE:AD:CE:EF:25:15 -netdev tap,id=mynet5,ifname=tap5,script=no,downscript=no \
	-device e1000,netdev=mynet6,mac=DE:AD:CE:EF:25:16 -netdev tap,id=mynet6,ifname=tap6,script=no,downscript=no \
	-device e1000,netdev=mynet7,mac=DE:AD:CE:EF:25:17 -netdev tap,id=mynet7,ifname=tap7,script=no,downscript=no \
	-device e1000,netdev=mynet8,mac=DE:AD:CE:EF:25:18 -netdev tap,id=mynet8,ifname=tap8,script=no,downscript=no \
	-vnc :1 -daemonize

