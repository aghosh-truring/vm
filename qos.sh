#!/bin/bash

IP=/usr/sbin/ip
TC=/usr/sbin/tc

declare -A DELAY
declare -A RATE
declare -A LOSS
declare -A UL
declare -A DL
declare -A NS

UL[wifi]=eno2;  DL[wifi]=U2;  NS[wifi]=S2;  DELAY[wifi]=3000;    RATE[wifi]=100000; LOSS[wifi]=1; 
UL[lte1]=eno3;  DL[lte1]=U3;  NS[lte1]=S3;  DELAY[lte1]=15000;   RATE[lte1]=50000;  LOSS[lte1]=1; 
UL[vsat1]=eno4; DL[vsat1]=U4; NS[vsat1]=S4; DELAY[vsat1]=250000; RATE[vsat1]=10000; LOSS[vsat1]=2; 
UL[lte2]=eno5;  DL[lte2]=U5;  NS[lte2]=S5;  DELAY[lte2]=15000;   RATE[lte2]=50000;  LOSS[lte2]=1; 
UL[vsat2]=eno6; DL[vsat2]=U6; NS[vsat2]=S6; DELAY[vsat2]=250000; RATE[vsat2]=10000; LOSS[vsat2]=2; 

while :
do
	for n in wifi lte1 vsat1 lte2 vsat2
	do
		__DELAY=${DELAY[$n]}
		__RATE=${RATE[$n]}
		__LOSS=${LOSS[$n]}

		FACTOR=$(( 100 + ($RANDOM % 50) ))
		__DELAY=$(( $__DELAY * $FACTOR / 100 ))

		FACTOR=$(( 100 - ($RANDOM % 33) ))
		__RATE=$(( $__RATE * $FACTOR / 100 ))

		${IP} netns exec ${NS[$n]} ${TC} qdisc change dev ${UL[$n]} root netem delay ${__DELAY}us rate ${__RATE}kbit loss ${__LOSS}%
		${IP} netns exec ${NS[$n]} ${TC} qdisc change dev ${DL[$n]} root netem delay ${__DELAY}us rate ${__RATE}kbit loss ${__LOSS}%
	done

	sleep 60
done
