#!/bin/bash

set -x 

cd /vm/device

for Q in *
do
	ip netns exec D${Q} wget -O /dev/null -q -b http://speedtest.tele2.net/10GB.zip
done
