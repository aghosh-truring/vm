#!/bin/bash

set -x 

cd /vm/device

Q=${1:?"Needs Parameter"}

if [ ! -d /vm/device/${Q} ]
then
	echo "No such device"
	exit 2
fi

if [ ! -f /vm/device/${Q}/vlan ]
then
	/vm/eapol_test -M  `cat /vm/device/${Q}/mac`  -N 30:s:88-88-22-22-22-22:foobar -c /vm/device/${Q}/user -a 10.0.254.1 | tail -1
fi

ip netns exec D${Q} dhcpcd -h D${Q} F${Q}
