#!/bin/bash

set -x

for N in 1 2 3 4 5 6 
do
	ip netns add S${N}
	ip netns exec S${N} ip addr add 127.0.0.1/8 dev lo
	ip netns exec S${N} ip link set lo up
	ip netns exec S${N} ip link add name T${N} type bridge
	# ip netns exec S${N} sysctl -w net.ipv4.conf.T${N}.arp_ignore=8
	ip netns exec S${N} ip link set T${N} up

	ip link add W${N} type veth peer name U${N}
	ip link set W${N} up

	ip addr add 172.21.${N}.1/24 dev W${N}
	busybox udhcpd /vm/dhcp/W${N}.conf

	ip link set U${N} netns S${N}
	ip netns exec S${N} ip link set U${N} up
	ip netns exec S${N} ip link set U${N} master T${N}

	ip link set tap${N} netns S${N}
	ip netns exec S${N} ip link set tap${N} up
	ip netns exec S${N} ip link set tap${N} master T${N}
done

