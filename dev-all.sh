#!/bin/bash

set -x 

cd /vm/device

for Q in *
do
	if [ ! -f /vm/device/${Q}/vlan ]
	then
		/vm/eapol_test -M  `cat /vm/device/${Q}/mac`  -N 30:s:88-88-22-22-22-22:foobar -c /vm/device/${Q}/user -a 10.0.254.1 | tail -1
	fi

	# ip netns exec D${Q} dhclient eth0
	ip netns exec D${Q} dhcpcd -h D${Q} F${Q}
done

# /vm/eapol_test -M 02:00:00:00:00:04  -N 30:s:88-88-22-22-22-22:foobar -c u1.conf -a 10.0.254.1
