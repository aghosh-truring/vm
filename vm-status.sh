#!/bin/bash

set -x 

cd /vm/device

ip -br l
ip -br a

for Q in *
do
	ip netns exec D${Q} ip -br a show dev F${Q}
done

