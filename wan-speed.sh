#!/bin/bash

IP="ip"

for q in 1 2 3 4 5 6
do
	N="ns"${q}
	A="vea"${q}
	B="veb"${q}

	TABLE="3"${q}
	BASE="172.23."${q}

	X=${BASE}".1"
	Y=${BASE}".2"

	${IP} netns add ${N}
	${IP} link add ${A} type veth peer ${B}

	${IP} link set ${B} netns ${N}
	${IP} addr add ${X}"/30" dev ${A}
	${IP} link set ${A} up

	${IP} rule add from ${Y} table ${TABLE}

	${IP} netns exec ${N} ${IP} add add 127.0.0.1/8 dev lo
	${IP} netns exec ${N} ${IP} link set lo up
	${IP} netns exec ${N} ${IP} addr add ${Y}"/30" dev ${B}
	${IP} netns exec ${N} ${IP} link set ${B} up
	${IP} netns exec ${N} ${IP} route add default via ${X}
done

# -- GW="172.20.2.1"
# -- ${IP} route add default via ${GW} table ${TABLE}

# --${IP} route add blackhole default table ${TABLE}
