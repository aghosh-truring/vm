#!/bin/sh

/vm/qemu-system-x86_64 -boot d -cdrom  alpine-virt-3.11.5-x86_64.iso -hda /vm/alpine2.qcow2 -name VM2  --enable-kvm  -m  4196 -cpu host -smp 4 \
	-device e1000,netdev=mynet9,mac=DE:AD:BE:EF:35:10 -netdev tap,id=mynet9,ifname=tap9,script=no,downscript=no \
	-device e1000,netdev=mynet10,mac=DE:AD:CE:EF:35:11 -netdev tap,id=mynet10,ifname=tap10,script=no,downscript=no \
	-device e1000,netdev=mynet11,mac=DE:AD:CE:EF:35:13 -netdev tap,id=mynet11,ifname=tap11,script=no,downscript=no \
	-device e1000,netdev=mynet12,mac=DE:AD:CE:EF:35:14 -netdev tap,id=mynet12,ifname=tap12,script=no,downscript=no \
	-vnc :2 -daemonize

