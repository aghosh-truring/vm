#!/bin/bash

set -x

# Common Part
sysctl -w net.ipv4.ip_forward=1

ip link add br0 type bridge
ip addr add 10.3.10.1/24 dev br0
ip link set br0 up

iptables -A FORWARD -i br0 -o ens160 -j ACCEPT
iptables -A FORWARD -i br0 -o ens160 -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -t nat -A POSTROUTING -o br0 -j MASQUERADE
iptables -t nat -A POSTROUTING -o ens160 -j MASQUERADE

# For VM 1
for N in 1 2 3 4 5 6 7 8 
do
	ip tuntap add mode tap tap${N}
done

for N in 1 2 3 4 5 6 
do
	if [ "$#" -eq "0" ]
	then
		ip link set tap${N} up
		ip addr add 172.20.${N}.1/24 dev tap${N}
		busybox udhcpd /vm/dhcp/tap${N}.conf
	fi
done

# The Management Port
ip link set tap8 master br0
ip link set tap8 up

iptables -t nat -A PREROUTING -i ens160 -p tcp --dport 7801 -j DNAT --to-destination 10.3.10.3:22
iptables -t nat -A PREROUTING -i ens160 -p tcp --dport 7811 -j DNAT --to-destination 10.3.10.3:80
iptables -t nat -A PREROUTING -i ens160 -p tcp --dport 7821 -j DNAT --to-destination 10.3.10.3:443

# The LAN Side
ip addr add 10.0.254.3/24 dev tap7
ip link set tap7 up

for V in 4093 10 20
do
	ip link add link tap7 name V${V} type vlan id ${V}
	ip link add name B${V} type bridge
	ip link set V${V} master B${V}
	sysctl -w net.ipv4.conf.B${V}.arp_ignore=8

	ip link set B${V} up
	ip link set V${V} up
done

# The Devices
cd /vm/device

for Q in *
do
	ip netns add D${Q}
	ip link add E${Q} type veth peer name F${Q}

	if [ -f /vm/device/${Q}/vlan ]
	then
		ip link set E${Q} master B`cat /vm/device/${Q}/vlan`
	else
		ip link set E${Q} master B4093
	fi
	ip link set E${Q} up

	# after this lin we can't see interface F in the main namespace
	ip link set F${Q} netns D${Q}

	MAC="02:CA:FE:00:00:${Q}"
	echo ${MAC} > /vm/device/${Q}/mac

	ip netns exec D${Q} ip link set F${Q} addr ${MAC}
	# ip netns exec D${Q} ip link set F${Q} name eth0
	# ip netns exec D${Q} ip link set eth0 up
	ip netns exec D${Q} ip link set F${Q} up

	ip netns exec D${Q} ip addr add 127.0.0.1/8 dev lo
	ip netns exec D${Q} ip link set lo up
done

# For VM 2 ##################################################################
for N in 9 10 11 12
do
	ip tuntap add mode tap tap${N}
done

ip link set tap9 master br0
ip link set tap9 up
